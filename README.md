## Prerequisites

Node.js >= 0.8


## Installation

Installs the dependency modules required by this application.

    npm install

## Run the application

Run the application locally, at [http://localhost:8666/]([http://localhost:8666/). 

    npm start
