/*jslint anon:true, sloppy:true, nomen:true*/

var path = require('path');

process.chdir(__dirname);

/*
 * Create the MojitoServer instance we'll interact with. Options can be passed
 * using an object with the desired key/value pairs.
 */
var Mojito = require('mojito');
var app = Mojito.createServer();

// ---------------------------------------------------------------------------
// Different hosting environments require different approaches to starting the
// server. Adjust below to match the requirements of your hosting environment.
// ---------------------------------------------------------------------------

module.exports = app.listen(null, null, function (err) {
    if (!err) {
        var appManifest = require(path.join(__dirname, '/package.json'));
        console.log("\n\u2713 " + appManifest.name + " v" + appManifest.version + " has started on http://127.0.0.1:8666/\n");
    }
});
