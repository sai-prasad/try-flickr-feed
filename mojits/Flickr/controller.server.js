/*global YUI*/

YUI.add('Flickr', function(Y, NAME) {

    "use strict";

    console.log(Y.mojito.models.FlickrModel);

    Y.namespace('mojito.controllers')[NAME] = {

        index: function(ac) {
            var model = ac.models.get('feed');
            model.getPublicFeed({}, function (err, data) {
                if (err) {
                    return ac.error(err);
                }
                ac.done({
                    data: data
                });
            });
        },

        user: function (ac) {
            var userid = ac.params.route('id');
            var model = ac.models.get('feed');
            model.getUserFeed(userid, function (err, data) {
                if (err) {
                    return ac.error(err);
                }
                ac.done({
                    userid: userid,
                    data: data
                });
            });
        },

        friends: function (ac) {
            var userid = ac.params.route('id');
            var model = ac.models.get('feed');
            model.getFriendsFeed(userid, function (err, data) {
                if (err) {
                    return ac.error(err);
                }
                ac.done({
                    userid: userid,
                    data: data
                });
            });
        }
    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'mojito-params-addon']});