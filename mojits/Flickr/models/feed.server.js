/*global YUI*/

YUI.add('FlickrModelFeed', function(Y, NAME) {
    "use strict";

    /**
     * Base URL for Flickr's Feeds.
     * @const
     */
    var FLICKR_BASE_FEED_URL = "http://api.flickr.com/services/feeds/";

    var REST = Y.mojito.lib.REST;

    Y.namespace('mojito.models')[NAME] = {
        
        getPublicFeed: function (params, callback) {
            var url = FLICKR_BASE_FEED_URL + "photos_public.gne";

            // Add parameters defining the format the data needs to be returned.
            if (!params) { params = {}; }
            params.format = "json";
            params.nojsoncallback = "1";

            REST.GET(url, params, {}, function (err, response) {
                if (err) {
                    return callback(err);
                }

                var data;

                try {
                    // HACK: Flickr's feed wrongly escapes single quote in JSON format, leading to invalid JSON.
                    // So, replace \' with '.
                    var body = response.getBody().replace(/\\\'/g, "'");
                    data = JSON.parse(body);
                } catch (ex) {
                    return callback(ex);
                }

                if (!data.items || !Array.isArray(data.items)) {
                    return callback(new Error("Unexpected response. data in response is not an array."));
                }

                return callback(null, data.items);
            });
        },

        getUserFeed: function (user, callback) {
            this.getPublicFeed({id: user}, callback);
        },

        getFriendsFeed: function(user, callback) {
            var url = FLICKR_BASE_FEED_URL + "photos_friends.gne";
            var params = {
                user_id: user,
                display_all: 1,
                format: "json",
                nojsoncallback: 1
            };

            REST.GET(url, params, {}, function (err, response) {
                if (err) {
                    return callback(err);
                }

                var data;

                try {
                    // HACK: Flickr's feed wrongly escapes single quote in JSON format, leading to invalid JSON.
                    // So, replace \' with '.
                    var body = response.getBody().replace(/\\\'/g, "'");
                    data = JSON.parse(body);
                } catch (ex) {
                    return callback(ex);
                }

                if (!data.items || !Array.isArray(data.items)) {
                    return callback(new Error("Unexpected response. data in response is not an array."));
                }

                return callback(null, data.items);
            });
        }
    };

}, '0.0.1', {requires: ['mojito-rest-lib']});